﻿using Iternity.PlantUML;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Iternity.plantuml.cs.tests
{
    [TestClass]
    public class PlantUMLUrlTests
    {
        private static string AliceAndBobSequence = "@startuml\nBob -> Alice : hello\n@enduml";

        [TestMethod]
        public void DefaultLicenseDiagram()
        {
            var url = PlantUMLUrl.Create().ToString();
            Assert.AreEqual("http://plantuml.com/plantuml/uml/SoWkIImgAStDuSh9J4xDAqxbSaZDIm7o0G00", url);
        }

        [TestMethod]
        public void BobAndAliceSequenceDiagramAsSVG()
        {
            var url = PlantUMLUrl.SVG(AliceAndBobSequence);
            Assert.AreEqual("http://plantuml.com/plantuml/svg/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void BobAndAliceSequenceDiagramAsPNG()
        {
            var url = PlantUMLUrl.PNG(AliceAndBobSequence);
            Assert.AreEqual("http://plantuml.com/plantuml/png/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void BobAndAliceSequenceDiagramAsUML()
        {
            var url = PlantUMLUrl.UML(AliceAndBobSequence);
            Assert.AreEqual("http://plantuml.com/plantuml/uml/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void BobAndAliceSequenceDiagramAsASCII()
        {
            var url = PlantUMLUrl.ASCII(AliceAndBobSequence);
            Assert.AreEqual("http://plantuml.com/plantuml/txt/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }

        [TestMethod]
        public void BobAndAliceSequenceDiagramWithLocalBaseUrl()
        {
            var url = PlantUMLUrl.Create().WithBaseUrl("http://localhost:8080/plantuml").UmlStyle().WithUmlContent(AliceAndBobSequence).ToString();
            Assert.AreEqual("http://localhost:8080/plantuml/uml/SoWkIImgAStDuNBAJrBGjLDmpCbCJbMmKiX8pSd9vt98pKi1IW80", url);
        }
    }
}
